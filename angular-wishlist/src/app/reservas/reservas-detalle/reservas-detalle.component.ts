import { Component, OnInit } from '@angular/core';
import { ReservasApiClientService } from '../reservas-api-client.service';

@Component({
  selector: 'app-reservas-detalle',
  templateUrl: './reservas-detalle.component.html',
  styleUrls: ['./reservas-detalle.component.scss']
})
export class ReservasDetalleComponent implements OnInit {

  constructor( private api: ReservasApiClientService) { }

  ngOnInit(): void {
  }

}
