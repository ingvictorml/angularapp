import { VoteUpAction, VoteDownAction } from '../../models/destinos-viajes-state.model';
import { Component, OnInit, Input, HostBinding, Output, EventEmitter } from '@angular/core';
import { DestinoViaje } from '../../models/destino-viaje.model';
import { Store } from '@ngrx/store';
import { AppState } from '../../app.module';
import { ResetVotesAction } from '../../models/destinos-viajes-state.model';
import { trigger, state, style, transition, animate} from '@angular/animations';

// Decorador
@Component({
  selector: 'app-destino-viaje',
  templateUrl: './destino-viaje.component.html',
  styleUrls: ['./destino-viaje.component.scss'],
  animations: [
    trigger('esFavorito', [
      state('estadoFavorito', style({
        backgroundColor: 'PaleTurquoise'
      })),
      state('estadoNoFavorito', style({
        backgroundColor: 'WhiteSmoke'
      })),
      transition('estadoNoFavorito => estadoFavorito', [
        animate('3s') // Delay
      ]),
      transition('estadoFavorito => estadoNoFavorito', [
        animate('1s')
      ]),
    ]
    )]
})
export class DestinoViajeComponent implements OnInit {
  @Input() destino: DestinoViaje; // Atributos
  // @Input() pais:string;
  @Input() position: number;
  @HostBinding('attr.class') cssClass = 'col-md-4';
  @Output() onClicked: EventEmitter<DestinoViaje>;

  constructor(private store: Store<AppState>) {
    this.onClicked = new EventEmitter();
  }

  ngOnInit(): void {
  }

  ir(): any{
    this.onClicked.emit(this.destino);
    return false;
  }

  voteUp(): any{
    this.store.dispatch(new VoteUpAction(this.destino));
    return false;
  }

  voteDown(): any{
    this.store.dispatch(new VoteDownAction(this.destino));
    return false;
  }

  resetVotes(): any{
    this.store.dispatch(new ResetVotesAction(this.destino));
    return false;
  }
}
