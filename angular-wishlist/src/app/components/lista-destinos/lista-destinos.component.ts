import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { DestinoViaje } from '../../models/destino-viaje.model';
import { DestinosApiClient } from '../../models/destinos-api-client.model';
import { Store } from '@ngrx/store';
import { AppState } from '../../app.module';

@Component({
  selector: 'app-lista-destinos',
  templateUrl: './lista-destinos.component.html',
  styleUrls: ['./lista-destinos.component.scss'],
  providers: [DestinosApiClient]
})
export class ListaDestinosComponent implements OnInit {
  @Output() onItemAdded: EventEmitter<DestinoViaje>;
  // paises:string[]=['USA','UE','Brazil','Mexico','Argentina']
  // destinos: DestinoViaje[];
  updates: string[];
  all: any;
  constructor(
    public destinosApiClient: DestinosApiClient,
    private store: Store<AppState>) {
    this.onItemAdded = new EventEmitter();
    this.updates = [];
    this.store.select(state => state.destinos.favorito)
    .subscribe(d => {
      if (d != null){
      this.updates.push('Se ha elegido a ' + d.nombre);
    }
    });
    store.select( state => state.destinos.items).subscribe(items => this.all = items);
  }

  ngOnInit(): void{
  }

  agregado(d: DestinoViaje): any{
    this.destinosApiClient.add(d);
    this.onItemAdded.emit(d);
  }

  elegido(e: DestinoViaje ): any{
    // this.destinosApiClient.getAll().forEach(x => x.setSelected(false));
    this.destinosApiClient.elegir(e);
  }
}
