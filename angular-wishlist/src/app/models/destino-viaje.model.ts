import {v4 as uuid} from 'uuid';

export class DestinoViaje {
  selected: boolean;
  servicios: string[];
  id1: string;
  id = uuid();

constructor(public nombre: string, public imagenUrl: string, public votes: number = 0){
    this.servicios = ['Pileta', 'Desayuno'];
}

isSelected(): boolean{
    return this.selected;
  }

setSelected(s: boolean): void {
    this.selected = s;
  }

voteUp(): any {
    this.votes++;
  }

voteDown(): any {
    this.votes--;
  }

resetVotes(): any {
    this.votes = 0;
  }
}
