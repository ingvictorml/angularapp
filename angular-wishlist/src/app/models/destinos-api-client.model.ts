import {Injectable, Inject, forwardRef} from '@angular/core';
import { DestinoViaje } from './destino-viaje.model';
import { Subject, BehaviorSubject } from 'rxjs';
import { tap, last } from 'rxjs/operators';
import { Action } from '@ngrx/store';
import { Store } from '@ngrx/store';
import {
    DestinosViajesState,
    NuevoDestinoAction,
    ElegidoFavoritoAction
  } from './destinos-viajes-state.model';
import {AppState, APP_CONFIG, AppConfig, db } from './../app.module';
import { HttpClient, HttpHeaders, HttpRequest, HttpResponse } from '@angular/common/http';

@Injectable()
export class DestinosApiClient {
  destinos: DestinoViaje[] = [];

  constructor( private store: Store<AppState>,
               @Inject(forwardRef(() => APP_CONFIG)) private config: AppConfig,
               private http: HttpClient
    ) {
    this.store
      .select(state => state.destinos)
      .subscribe((data) => {
        console.log('destinos sub store');
        console.log(data);
        this.destinos = data.items;
      });
    this.store
      .subscribe((data) => {
        console.log('all store');
        console.log(data);
      });
  }

  add(d: DestinoViaje): any {
    // aqui invovamos al constructor
    const headers: HttpHeaders = new HttpHeaders({'X-API-TOKEN': 'token-seguridad'});
    const req = new HttpRequest('POST', this.config.apiEndpoint + '/my', {nuevo: d.nombre}, { headers});
    this.http.request(req).subscribe((data: HttpResponse<{}>) => {
      if (data.status === 200) {
        this.store.dispatch(new NuevoDestinoAction(d));
        const myDb = db;
        myDb.destinos.add(d);
        console.log('todos los destinos de la db!');
        myDb.destinos.toArray().then(destinos => console.log(this.destinos));
      }
    });
  }

  getById(id: string): DestinoViaje {
    // tslint:disable-next-line: only-arrow-functions
    return this.destinos.filter((d) => { return d.id.toString() == id; })[0];
  }

  getAll(): DestinoViaje[] {
    return this.destinos;
  }

  elegir(d: DestinoViaje): any {
    // aqui incovariamos al servidor
    this.store.dispatch(new ElegidoFavoritoAction(d));
  }
}
